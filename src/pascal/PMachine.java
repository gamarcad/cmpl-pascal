package pascal;


import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class PMachine {
    //LiaisonSerie liaisonSerie;
    Instruction pcode[];
    int mem[] = new int[1000];
    int base = 0;
    int SP = 0;
    int PC = 0;
    boolean run = false;
    private Environement env;
    private Map< String, Object > registers;
    private StringBuilder output;


    public PMachine( Environement env, Instruction[] pcode )
    {
        this.env = env;
        this.pcode = pcode;
        this.registers = new TreeMap<>();
        this.output = new StringBuilder();
    }

    public void exec() {
        run = true;
        while(run) {
            // display instructions and stack
            StringBuilder builder = new StringBuilder();
            builder.append( pcode[ PC ] + " \twith stack: " + this.detailStack( env ) );


            System.out.println( builder.toString() );
            pcode[ PC ].exec( this );
        }
    }

    public <T> Optional<T> getRegister( String name ) {
        if ( this.registers.containsKey( name ) ) {
            return Optional.of( (T) this.registers.get( name ) );
        } else {
            return Optional.empty();
        }
    }

    public void setRegister( String name, Object data ) {
        this.registers.put( name, data );
    }

    public void output( Object o ) {
        this.output( o.toString() );
    }

    public void output( String s ) {
        this.output.append( s );
    }

    public String getOutput() {
        return this.output.toString();
    }

    public String toString() {
        StringBuffer res = new StringBuffer();
        for (int i=0; i< pcode.length; i++)
            res.append(i + "\t" + pcode[i] +"\n");
        return res.toString();
    }

    private String detailStack( Environement env ) {
        String res = "";
        for ( int i = 0; i < this.SP; ++i ) {
            // check if stack index is affected to a stack
            String s = "";
            int start = -1;
            int end = -1;
            for ( Map.Entry< String, Variable > symbols : env.getSymbols().getSymbols().entrySet() ) {
                String symbol = symbols.getKey();
                Variable data = symbols.getValue();
                int symbolAddress = data.adresse;

                if ( symbolAddress <= i && i < symbolAddress + data.getType().getTaille() ) {
                    s = symbol;
                    start = symbolAddress;
                    end = symbolAddress + data.getType().getTaille();
                }
            }

            if ( i == start && i == end - 1 ) {
                res += s + "( " + this.mem[ i ] + " ) ";
                continue;
            }
            if ( i == start ) {
                res += s + "( " + this.mem[ i ] + " ";
            } else if ( i == end - 1 ) {
                res += this.mem[ i ] + " ) ";
            } else {
                res += this.mem[ i ] + " ";
            }

        }
        return res;
    }
}
