package pascal;

import java.util.ArrayList;
import java.util.Collection;

public class Environement {
    private SymbolsTable symbolsTable;
    private Collection< Instruction > instructions;
    private ParserErrorManager errorsManager;


    public Environement() {
        this.symbolsTable = new SymbolsTable( this );
        this.instructions = new ArrayList<>();
        this.errorsManager = new ParserErrorManager();
    }

    public SymbolsTable getSymbols() {
        return this.symbolsTable;
    }

    public SymbolsTable getSymbolsTable() {
        return symbolsTable;
    }

    public Collection< Instruction > getInstructions() {
        return instructions;
    }



    public boolean hasError() {
        return this.errorsManager.hasErrors();
    }

    public ParserErrorManager getErrorsManager() {
        return errorsManager;
    }

    public void addInstructions( Instruction... instructions ) {
        for ( Instruction instruction : instructions ) {
            this.instructions.add( instruction );
        }
    }

    public void requireSameParameterType( String symbol, Type expressionType, int index ) {
        if ( symbolsTable.symbolExists( symbol ) && symbolsTable.getType( symbol ).callable() ) {
            Type.Proc proc = ( Type.Proc ) symbolsTable.getType( symbol );
            if ( index < proc.countParameters() ) {
                Type paramType = proc.getParamType( index );
                this.requireSameType( paramType, expressionType,
                        ( t1, t2 ) -> "Illegal parameter type on " + symbol + " call:  Required " + t1 + " but got " + t2 + "." );
            }
        }
    }

    public interface SingleErrorFormatter {
        String format( String type );
    }


    public interface DoubleErrorFormatter {
        String format( String type1, String type2 );
    }


    public void requireBoolType( Type expressionType ) {
        if ( !expressionType.getClass().equals( Type.Bool.class )) {
            this.errorsManager.addError( "Cannot evaluate " + expressionType.getClass().getSimpleName() + " as " + Type.Bool.class.getSimpleName() );
        }
    }

    public Type requireSameType( Type expressionType, Type expressionType1 ) {
        return requireSameType( expressionType, expressionType1, ( t1, t2 ) -> "Types " + t1 + " and " + t2 + " are not the same." );
    }

    public Type requireSameType( Type expressionType, Type expressionType1, DoubleErrorFormatter errorFormatter ) {
        if ( expressionType.getClass().equals( expressionType1.getClass() ) ) {
            return expressionType;
        } else {

            this.errorsManager.addError( errorFormatter.format( expressionType.getClass().getSimpleName(), expressionType1.getClass().getSimpleName() ) );
            return new Type.Unkown();
        }
    }

    public Type requireValidComparison( Type expressionType, Type expressionType1 ) {
        if ( expressionType.getClass().equals( expressionType1.getClass() ) ) {
            if ( expressionType.getClass().equals( Type.Entier.class ) ) {
                return expressionType;
            } else {
                this.errorsManager.addError( "Cannot compare objects which are not " + Type.Entier.class.getSimpleName() );
            }
        } else {
            this.errorsManager.addError( "Types " + expressionType.getClass().getSimpleName() + " and " + expressionType1.getClass().getSimpleName() + " are not the same." );
        }
        return new Type.Unkown();
    }

    public Type requireValidOperation( Type expressionType, Type expressionType1 ) {
        if ( expressionType.getClass().equals( expressionType1.getClass() ) ) {
            if ( expressionType.getClass().equals( Type.Entier.class ) ) {
                return expressionType;
            } else {
                this.errorsManager.addError( "Cannot compare objects which are not " + Type.Entier.class.getSimpleName() );
            }
        } else {
            this.errorsManager.addError( "Types " + expressionType.getClass().getSimpleName() + " and " + expressionType1.getClass().getSimpleName() + " are not the same." );
        }
        return new Type.Unkown();
    }
}
