//######################################################################################################################
// File: Pascal.g4
// Description: Pascal grammar
//######################################################################################################################

// grammar definition
grammar Pascal;

// Included dependancies in parser
@header {
    import java.util.*;
    import pascal.*;
    import static pascal.Instruction.*;
    import static pascal.Type.*;
}

@parser::members {
    Environement env = new Environement();
    TypesTable typesTable = new TypesTable();
    SymbolsTable table =  env.getSymbolsTable();
    Collection<pascal.Instruction> pCode = env.getInstructions();
    ParserErrorManager errorsManager = env.getErrorsManager();


    boolean pasErreur = true;
    public pascal.Environement parse() throws Exception {
        program();
        return env;
    }


    //TODO delete this (but might be impossible)
    int index;
    pascal.Type.Proc proc;

    // Constants
    public static final int UNDEFINED_ADDRESS = -1;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PASCAL PROGRAM STRUCTURE DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
program:
    commentary*
    'program' ID ';'
    block
    '.'
    { pCode.add( new HLT() ); };


block:
    // types definitions: can be omitted
    deftypes?

    // variables definition: can be omitted
    vars?
    {
        pCode.add(new pascal.Instruction.INC(table.getTaille()));
        pascal.Instruction.BRN brn = new pascal.Instruction.BRN(-1);
        pCode.add(brn);
    }

    // procedures definition: can be omitted
    defprocs?
    {
        brn.setParam(pCode.size());
    }

    // instructions
    'begin'
        instructions_block*
    'end' ;

instructions_block :
    commentary |
    statement |
    instruction ';' commentary? |
    'begin' ( instructions_block )* 'end';


instruction: appelproc | affect | read | write | halt_exception;

statement: loop_statement | condition_statement | switch_statement | for_loop_statement;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CONDITIONAL STATEMENTS DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Conditionnal statement
condition_statement:
    'if' expression
    { BZE bze = new BZE( UNDEFINED_ADDRESS ); env.addInstructions( bze ); }
    'then' instructions_block
    { bze.setParam( pCode.size() ); }
    |
    'if' expression
    { BZE bze = new BZE( UNDEFINED_ADDRESS ); env.addInstructions( bze ); }
    'then' instructions_block
    { BRN brn = new BRN( UNDEFINED_ADDRESS ); env.addInstructions( brn ); }
    'else'
    { bze.setParam( pCode.size() );  }
    instructions_block
    { brn.setParam( pCode.size() );  };

switch_statement:
    { BRN brn = new BRN( UNDEFINED_ADDRESS ); }
    'switch' expression ':'
        (
            { env.addInstructions( new CTS() ); }
            'case' expression ':'
            { BZE bze = new BZE( UNDEFINED_ADDRESS ); env.addInstructions( new EQL(), bze ); }
            instructions_block
            { env.addInstructions( brn ); bze.setParam( pCode.size() );  }
        )+
        'end'
        { brn.setParam( pCode.size() ); };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// LOOP STATEMENTS DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
loop_statement: while_loop_statement | for_loop_statement | repeat_loop_statement;

// While loop
while_loop_statement:
    'while' {  int while_start = pCode.size(); }
    expression
    { BZE bze = new BZE( UNDEFINED_ADDRESS ); pCode.add( bze ); }
    'do' instructions_block
    {
        pCode.add( new BRN( while_start ) );
        bze.setParam( pCode.size() );
    };

// 'repeat' <instructions>* 'until' <expression>
repeat_loop_statement:
    { int start = pCode.size(); }
    'repeat'  instructions_block 'until' expression
    { env.addInstructions( new BZE( start ) ); };

// for loop
for_loop_statement:
    'for' address
    { int ad = $address.location; }
    'from' expression
    { pCode.add(new pascal.Instruction.STO(1)); int debut = pCode.size(); }
    'to'expression
    {
        BZE bze = new BZE( UNDEFINED_ADDRESS );
        env.addInstructions( new LDA(ad), new LDV(1), new GEQ(), bze );
    }
    'do' instructions_block
    {
        env.addInstructions( new LDA(ad), new LDA(ad), new LDV(1), new LDI(1), new ADD(), new STO(1), new BRN(debut));
        bze.setParam( pCode.size() );
    };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DOCUMENTATION INSTRUCTIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
commentary: COMMENT;
COMMENT: '//' ( 'A'..'Z' | 'a' .. 'z' | '0' .. '9' | ' ' | 'é' | 'è' | '.' | '+' | ',' | 'à' | '\'' | ':' | '=' | ';' | '/' | '(' | ')' )*;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HALT INSTRUCTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
halt_exception:
    'exit()' { env.addInstructions( new Instruction.HLT() ); } |
    'exit(' statusCode = INT ')' { env.addInstructions( new Instruction.HLT( $statusCode.int ) ); };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PROCEDURE DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
defprocs:  ( defproc )+;
defproc:
    commentary*
    // procedure definition
    'proc' proc_name = ID
    {
        pascal.Type.Proc proc = new pascal.Type.Proc();
        table.putProc($proc_name.text,proc,pCode.size());
        table.downLevel();
    }

    // procedure's parameters
    '('
        (
            d = defparams
            {
                proc.param = $d.tytype;
                for (pascal.Type.Parametre p:proc.param)
                    proc.taille += p.getTaille();
                int ad = -proc.taille-2;
                for (int i=0; i<proc.param.size(); i++) {
                    table.putParametre($defparams.id.get(i), proc.param.get(i),ad);
                    ad += proc.param.get(i).getTaille();
                }
            }
        )?
    ')'

    // returns type
    ':' type
    { proc.setReturnedType( $type.tytype ); }

    // procedure's local variables
    vars?
    { pCode.add(new pascal.Instruction.INC(table.getTaille())); }

    // procedure body
    'begin'
        (
            defproc |
            statement |
            instruction ';' |
            commentary |
            {
                env.addInstructions( new RETSTACK() );
            }
            'return'  expression ';'
            {
                env.requireSameType(
                    proc.getReturnedType(), $expression.expressionType,
                    ( t1, t2 ) -> "Invalid returned type in procedure " + $proc_name.text + ": returned " + t1 + " but required " + t2 + "."  );
                env.addInstructions( new RETVALUE( proc.parametersSize() ) );
            }
        )*

    'end'
    {
        pCode.add(new pascal.Instruction.RET(0));
        table.upLevel();
    };



// procedure's parameters declaration
defparams returns [ ArrayList<pascal.Type.Parametre> tytype, ArrayList<String> id ]:
    {
        $tytype = new ArrayList<pascal.Type.Parametre>();
        $id = new ArrayList<String>();
    }
    p0=defparam
    {
        $tytype.add($p0.tytype);
        $id.add($p0.id);
    }
    (
        ',' p1=defparam
        {
            $tytype.add($p1.tytype);
            $id.add($p1.id);
        }
    )*;

// procedure's parameter declaration
defparam returns[pascal.Type.Parametre tytype, String id] :
    // parameter's name and type
    'var' ID ':' type
    {
        $tytype = new pascal.Type.Parametre($type.tytype,false);
        $id = $ID.text;
    }
    |
    ID ':' type
    {
        $tytype = new pascal.Type.Parametre($type.tytype,true);
        $id = $ID.text;
    };



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TYPE DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
deftypes : commentary? 'type' ( deftype )+;
deftype :
    ID '=' type
    { typesTable.registerType( $ID.text, $type.tytype ); };

type returns[ Type tytype ]:
    'void'
    { $tytype = new pascal.Type.Void(); }
    |
    'integer'
    { $tytype= new pascal.Type.Entier(); }
    |
    'array' '[' INT ']' 'of' type
    { $tytype = new pascal.Type.Tableau( $INT.int, $type.tytype ); }
    |
    'record'
    {
        Enregistrement enregistrement = new Enregistrement();
    }
    (
        champ ';'
        {
            enregistrement.addField( $champ.nom, $champ.tytype );
        }
    )*
    'end'
    { $tytype = enregistrement; }
    |
    ID
    { $tytype = typesTable.getType( $ID.text ); };

champ returns[String nom, pascal.Type tytype]:
    ID ':' type
    { $nom = $ID.text; $tytype = $type.tytype; };



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
vars : commentary* 'var' ( var ';' )+;
var:
    x=ID
    {
       ArrayList<String> mesVar = new ArrayList<String>();
       mesVar.add($x.text);
    }
    (
        ',' y = ID
        { mesVar.add($y.text); }

    )* ':' type
    {
        for ( String v : mesVar ) {
            table.put(v,$type.tytype);
        }
    }
    ;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PROCEDURE CALLING DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
appelproc returns [ Type returnedType ]
    :
    // procedure's name
    procedure_name = ID
    {
        Variable v = null;
        table.requireSymbolExists( $procedure_name.text );
        table.requireSymbolAsProcedure( $procedure_name.text );
        if ( table.symbolExists( $procedure_name.text ) && table.getType( $procedure_name.text ).callable()  ) {
            v= table.get( $procedure_name.text );
            proc = (pascal.Type.Proc) v.type;
            $returnedType = proc.getReturnedType();
        } else {
            $returnedType = new Type.Unkown();
        }
        index = 0;
    }

    // procedure's parameters
    '('
        (
            callparam
            {  env.requireSameParameterType( $procedure_name.text, $callparam.expressionType, index );  index++; }
            (
                ',' callparam
                { env.requireSameParameterType( $procedure_name.text, $callparam.expressionType, index ); index++; }

            )*
        )?
        {
            table.requireValidNumberOfParameters( $procedure_name.text, proc, index );
        }
    ')'

    // increase space memory required to store parameters value or address
    {
        if ( !env.hasError() ) {
            pCode.add( new pascal.Instruction.CAL( v.adresse ) );
        }
    };

callparam returns [ Type expressionType ]:
    { proc != null && proc.paramHasValue( index ) }?
    expression { $expressionType = $expression.expressionType; }
    |
    address
    {
        table.requireParamHasType( proc, index, $address.tytype );
        $expressionType = $address.tytype;
    }
    ;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// INPUT AND OUTPUT FUNCTIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
read : 'read' '(' x=address ')' {
            pCode.add(new pascal.Instruction.INN());
            pCode.add(new pascal.Instruction.STO(1));
};

write : 'write' '('expression')' {
            pCode.add(new pascal.Instruction.PRN());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// AFFECTATION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
affect:
    address ':=' expression
    {
        env.requireSameType( $address.tytype, $expression.expressionType,
            ( t1, t2 ) -> "Illegal affectation: Cannot cast " + t2 + " as " + t1 + "." );
        if ( !env.hasError() ) {
            // first we need to get symbol's type located at given address
            pCode.add( new pascal.Instruction.STO( $address.tytype.getTaille() ) );
        }

    };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// EXPRESSION DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
expression returns[ Type expressionType ]:
    x = simple_expression  { $expressionType = $x.expressionType; }
    |
    'not' x = simple_expression { $expressionType = $x.expressionType; env.addInstructions( new NOT() ); }
    |
    cond = simple_expression { BZE bze = new BZE( UNDEFINED_ADDRESS ); env.addInstructions( bze ); env.requireBoolType( $cond.expressionType );   }
    '?' left = expression { BRN brn = new BRN( UNDEFINED_ADDRESS ); env.addInstructions( brn ); bze.setParam( pCode.size() ); }
    ':'  right = expression { brn.setParam( pCode.size() );  }
     { $expressionType = env.requireSameType( $left.expressionType, $right.expressionType ); }
     |
    l = simple_expression '==' r = simple_expression { pCode.add( new Instruction.EQL() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); } |
    l = simple_expression '!=' r = simple_expression { pCode.add( new Instruction.NEQ() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); } |
    l = simple_expression '<' r = simple_expression { pCode.add( new Instruction.LSS() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); } |
    l = simple_expression '<=' r = simple_expression { pCode.add( new Instruction.LEQ() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); } |
    l = simple_expression '>' r = simple_expression { pCode.add( new Instruction.GTR() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); } |
    l = simple_expression '>=' r = simple_expression { pCode.add( new Instruction.GEQ() ); $expressionType = env.requireValidComparison( $l.expressionType, $r.expressionType ); }
    ;



simple_expression returns [ Type expressionType ]:
    t = term { $expressionType = $t.expressionType; } |
    l = term '||' r = simple_expression { $expressionType = new Type.Bool(); env.addInstructions( new OR() ); } |
    l = term '-' r = simple_expression { pCode.add( new Instruction.SUB() ); $expressionType = env.requireValidOperation( $l.expressionType, $r.expressionType ); } |
    l = term '+' r = simple_expression { pCode.add( new Instruction.ADD() ); $expressionType = env.requireValidOperation( $l.expressionType, $r.expressionType ); };

term returns[ Type expressionType ]:
    f = factor { $expressionType = $factor.expressionType; } |
    '(' expression ')' { $expressionType = $expression.expressionType; } |
    l = term '&&' simple_expression { $expressionType = new Type.Bool(); env.addInstructions( new AND() ); } |
    l = term '*' r = simple_expression { pCode.add( new Instruction.MUL() ); $expressionType = env.requireValidOperation( $l.expressionType, $r.expressionType ); } |
    l = term '%' r = simple_expression { pCode.add( new Instruction.MOD() ); $expressionType = env.requireValidOperation( $l.expressionType, $r.expressionType ); } |
    l = term '/' r = simple_expression { pCode.add( new Instruction.DIV() ); $expressionType = env.requireValidOperation( $l.expressionType, $r.expressionType ); };

factor returns [ Type expressionType ]:
    FALSE { pCode.add( new Instruction.LDI( 0 ) ); $expressionType = new Type.Bool();}
    |
    TRUE  { pCode.add( new Instruction.LDI( 1 ) ); $expressionType = new Type.Bool(); }
    |
    INT   { pCode.add( new Instruction.LDI( $INT.int ) ); $expressionType = new Type.Entier(); }
    |
    '-' INT   { pCode.add( new Instruction.LDI( $INT.int * -1 ) ); $expressionType = new Type.Entier(); }
    |
    '['
        INT
        { pCode.add( new Instruction.LDI( $INT.int ) ); }
        (
            ',' INT
            { pCode.add( new Instruction.LDI( $INT.int ) ); }
        )*
    ']'
    { $expressionType = new Type.Tableau( -1, new Type.Entier() ); }
    |
    address
    {
        if ( $address.tytype != null ) {
            pCode.add(new pascal.Instruction.LDV($address.tytype.getTaille()));
            $expressionType = $address.tytype;
        } else {
            $expressionType = new Type.Unkown();
        }
    }
    |
    appelproc
    {
        $expressionType = $appelproc.returnedType;
    };

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ADDRESS DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
address returns[ pascal.Type tytype, int location ]:
    object = ID
    {
        table.requireSymbolExists( $object.text );
        if ( table.symbolExists( $object.text ) ) {
            pascal.Variable v = table.get( $object.text );
            if (v.type instanceof pascal.Type.Parametre) {
                pascal.Type.Parametre p = (pascal.Type.Parametre) v.type;
                $tytype = p.type;
                $location = v.adresse;
                pCode.add(new pascal.Instruction.LDL($location));
                if (!p.parValeur)
                    pCode.add(new pascal.Instruction.LDV(1));
            } else {
                $tytype = v.type;
                $location = v.adresse;
                if (v.isGlobal)
                    pCode.add(new pascal.Instruction.LDA($location));
                else
                    pCode.add(new pascal.Instruction.LDL($location));
            }
        }
    }
    (
        '[' expression ']'
        {
            table.requireSymbolAsArray( $object.text );
            if ( !env.hasError() ) {
                pascal.Type.Tableau t= (pascal.Type.Tableau) $tytype;
                $tytype =  t.type;
                if (t.type.getTaille() !=1 ) {
                    pCode.add(new pascal.Instruction.LDI(t.type.getTaille()));
                    pCode.add(new pascal.Instruction.MUL());
                }
                pCode.add(new pascal.Instruction.ADD());
            }
        }
        |
        '.' field = ID
        {
            // firstly, we must ensure that accessed object is a record
            table.requireSymbolAsRecord( $object.text );
            if ( !env.hasError() ) {
                pascal.Type.Enregistrement recordType = (pascal.Type.Enregistrement) $tytype;

                // secondly, accessde fields must exists in record
                table.requireRecordFieldExists( $object.text, recordType, $field.text );
                if ( !env.hasError() ) {
                    $tytype =  recordType.getType( $field.text );
                    pCode.add(new pascal.Instruction.LDI( recordType.getAdresse( $field.text) ) );
                    pCode.add(new pascal.Instruction.ADD());
                }
            }
        }
    )*
    // prevent returned type at null if failed by returning Unkown type
    {
        if ( $tytype == null ) {
            $tytype = new Type.Unkown();
        }
    }
    ;

TRUE: 'TRUE';
FALSE: 'FALSE';
ID:[a-zA-Z_][a-zA-Z0-9_]*;
INT: [1-9][0-9]*|[0];
WS : [ \r\t\n]+ -> skip;

